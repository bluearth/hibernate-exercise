package exercise.agenda;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exercise.agenda.model.Calendar;
import exercise.agenda.model.Event;
import exercise.agenda.model.Person;
import exercise.agenda.model.RecurringEvent;

public class AgendaApp {

	static Logger logger = LoggerFactory.getLogger(AgendaApp.class);
	
	public static void main(String[] args) throws Exception {
		Configuration config = new Configuration();
		
		config.setProperty("hibernate.connection.driver_class", "org.apache.derby.jdbc.EmbeddedDriver");
		config.setProperty("hibernate.connection.url","jdbc:derby:data/agendaApp;create=true");
		config.setProperty("hibernate.connection.username","");
		config.setProperty("hibernate.connection.password","");
		config.setProperty("hibernate.connection.pool_size","5");
		config.setProperty("hibernate.show_sql","false");
		config.setProperty("hibernate.format_sql","true");
		config.setProperty("hibernate.hbm2ddl.auto","create");
		config.setProperty("hibernate.hbm2ddl.import_files","import.sql");		
		config.addAnnotatedClass(Person.class);
		config.addAnnotatedClass(Calendar.class);
		config.addAnnotatedClass(Event.class);
		config.addAnnotatedClass(RecurringEvent.class);
		
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(config.getProperties())
				.build();
		
		SessionFactory sessionFactory = config.buildSessionFactory(serviceRegistry);		
		// Use sessionFactory to open session each time needed
		Session session = sessionFactory.openSession();

		
		
		session.flush();
		session.close();
		sessionFactory.close();

	}

}
